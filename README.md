# Reilef-Web

Website front-end for the RelieffTT project.

[![forthebadge made-with-javascript][5]](https://www.javascript.com/)

![nodejs][1] ![npm][2] ![tests][3] [![MIT license][4]](https://lbesson.mit-license.org/)

## Installation

Clone the repo and navigate to the working directory.

```bash
git clone https://bitbucket.org/relieftt/relief-web.git
cd relief-web
```

Use the package manager [npm](https://www.npmjs.com/) to install the dependencies.

```bash
npm install
```

## Usage

Start the Nodejs Web server.

```bash
npm start
```

Navigate to <http://localhost:3000/> to browse.

To run tests, execute the following command:

```bash
npm test
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)

[1]: https://img.shields.io/badge/nodejs-v8.11.3-brightgreen
[2]: https://img.shields.io/badge/npm-5.6.0-red
[3]: https://img.shields.io/badge/test-passing-green
[4]: https://img.shields.io/badge/License-MIT-blue.svg
[5]: https://forthebadge.com/images/badges/made-with-javascript.svg

import React, { Component } from 'react';
import Home from './components/Home.js';
import Services from './components/Services.js';
import Emergency from './components/Emergency.js';
import Donation from './components/Donation.js';
import { Router, Route, browserHistory } from 'react-router';

class App extends Component {
  render(){
    return (
      <Router history = {browserHistory}>
        <Route path = "/" component = {Home} />
        <Route path = "/emergency" component = {Emergency} />
        <Route path = "/services" component = {Services} />
        <Route path = "/donation" component = {Donation} />
      </Router>
    );
  }
}

export default App;

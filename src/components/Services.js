import React, { Component, Fragment } from 'react';
import { Header, Footer } from './Layouts';

class Services extends Component {
   render() {
      return (
         <Fragment>
            <Header/>
               <main class="content">
                  <section class="hero">
                     <img class="hero-image" src="assets/services.png" alt="Services for Victims"/>
                     <div class="hero-right">
                        <div class="hero-content">
                           <h2>Services for Victims</h2>
                           <div class="hr-styled"></div>
                           <p>Find or add services being offered to victims and the relief effort.</p>
                     </div>
                     <img class="hero-flag" src="assets/tt-flag-header.png" alt="TT Flag"/>
                     </div>
                  </section>
                  <section class="relief-list">
                     <h3 class="section-title">Disaster Relief Assistance</h3>
                     <div class="container">
                        <p>Disaster relief is provided to victims whose items were destroyed beyond use due to natural or man-made
                           disasters. The Ministry is collaborating with the Office of Disaster Preparedness and Management (ODPM), to
                           coordinate disaster relief to families affected by the floods.
                           Affected victims should contact the first responders at the Regional Corporation, via their Disaster
                           Management
                           Unit (DMU) within the respective areas.
                        </p>
                     </div>
                     <div class="spacer"></div>
                     <h3 class="section-title">Social Welfare Grants for Disaster Relief</h3>
                        <div class="container">
                           <h4>1. Household Items</h4>
                           <p>
                              Up to $10,000.00.
                           </p>
                           <p><strong>Requirements:</strong> ID Card, estimate for
                              furniture.</p>
                           <br/>
                           <p>Household Items include:
                              <ul>
                                 <li>Refrigerator</li>
                                 <li>Washing Machine</li>
                                 <li>Stove</li>
                                 <li>Chest of Drawers</li>
                                 <li>Bed</li>
                                 <li>Living Room Set</li>
                                 <li>Wardrobe</li>
                                 <li>Kitchen Cupboard</li>
                                 <li>Dining Room Set</li>
                              </ul>
                           </p>
                           <div class="spacer"></div>
                           <h4>
                              2. Clothing Grant
                           </h4>
                           <p>$1,000.00 per person.</p>
                           <p><strong>Requirements:</strong> ID Card, Estimate from supplier, DMU Report where applicable.</p>
                           <div class="spacer"></div>
                           <h4>
                              3. School Supplies
                           </h4>
                           <p>
                              <ul>
                                 <li>Primary School - $700.00</li>
                                 <li>Secondary School - $1,000.00</li>
                              </ul>
                           </p>
                           <p>This grant is intended to assist children who are attending primary and secondary schools whose
                              books/uniforms were destroyed.</p>
                           <p>
                              <strong>Requirements:</strong> ID Card, Estimate from supplier, DMU Report where applicable.
                           </p>

                        <div class="spacer"></div>
                        <h4>
                           4. Relief for Repairs to Home via National Social Development Programme (NSDP)
                        </h4>
                           <ul>
                              <li>Minor House Repair Assistance</li>
                              <li>Materials for Sanitary Plumbing Assistance</li>
                              <li>House Wiring Assistance</li>
                              <li>Improved Water Supplies</li>
                           </ul>
                           <p>
                              The value of the grant is $20,000.00.
                           </p>
                        <div class="spacer"></div>
                        <h4>
                           5. Food Support Programme
                        </h4>
                        <ul>
                           <li>1-3 persons - $410.00</li>
                           <li>4-5 persons - $550.00</li>
                           <li>6 persons and above - $700.00</li>
                        </ul>

                        <p><strong>Requirements:</strong> ID card, Applicant's birth certificate , Personal Identification Number
                           (PIN), Birth
                           certificates for all members of the applicant's household , Two (2) recent passport-sized photos,
                           Recent
                           utility bill (telephone, electricity, water)</p>
                        </div>
                     <div class="spacer"></div>
                     <h3 class="section-title">Pychosocial Support</h3>
                     <div class="container">
                        <p>
                           Psychosocial and Counselling Support from National Family Services Division Social Workers attached to the
                           National Family Services Division are ready to provide psychosocial support and counselling to affected
                           families to help them cope with and manage stress during the period.
                        </p>
                        <p>
                           <strong>CONTACT INFORMATION:</strong>
                           <br/>
                           623-2608 ext. 1601-1607
                           <br/>
                           Ministry of Social Development and Family Services
                           <br/>
                           CL Financial Building,
                           <br/>
                           39-43 St. Vincent Street,
                           <br/>
                           Port of Spain.
                           <br/>
                           Tel: (868) 623-2608
                           <br/>
                           Toll Free 800-(1MSD) 1673
                        </p>
                     </div>
                  </section>
               </main>
            <Footer/>
         </Fragment>
      );
   }
}

export default Services;
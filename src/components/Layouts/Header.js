import React, { Component } from 'react';

class Header extends Component {
   render() {
      return (
         <header class="header">
            <nav class="navbar navbar-expand-md navbar-light bg-primary">
               <a class="navbar-brand" href="/"><img src="assets/logo.png" alt="Relief TT Logo"/></a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-links" aria-controls="menuLinks" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="menu-links">
                     <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                           <a class="nav-link" href="/">Home<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="/emergency">Emergency Contacts</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="/services">Services For Victims</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="/donation">Donation Links</a>
                        </li>
                     </ul>
                     <a href="/#">
                        <button class="btn btn-lg btn-warning add-location" type="submit">Add New Location</button>
                     </a>
                  </div>
            </nav>
         </header>
      );
   }
}

export default Header;
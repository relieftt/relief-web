import React, { Component } from 'react';

class Footer extends Component {
   render() {
      return (
         <footer class="footer-content">
            <section class="disclaimer">
               <h5>Disclaimer</h5>
               <h3>Communicate | Confirm | Coordinate</h3>
               <p>This list is created by public contributions,
               and managed voluntarily by the <br/><strong>Caribbean Developers Group</strong>.</p>
               <p>While listing additions are verified,
               we do recommend calling to confirm details using
               the contact information provided, as we cannot be responsible for how quickly
               notice on the CLOSING of locations or services is distributed or moderated.</p>
            </section>
            <section class="contact">
               <img src="assets/logo-color.png" alt="Relief TT Logo Color"/>
               <p>If you need to urgently reach us to update or modify an entry,
               please call us at <a href="tel:1-868-354-3015"> 1-868-354-3015</a> for list management.</p>
               <p>If you would like to support the growth of this project please email
               <a href="mailto:info@kendallarneaud.me">info@kendallarneaud.me</a>
               <br/>OR <a href="mailto:designchangett@gmail.com">designchangett@gmail.com.</a></p>
            </section>
            <nav class="navbar navbar-dark justify-content-center text-secondary">
            <span>Built by Relief TT | Copyright {new Date().getFullYear()} Relief TT. All Rights Reserved.</span>
            </nav>
         </footer>
      );
   }
}

export default Footer;
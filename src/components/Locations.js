import React, { Fragment } from 'react';

const Locations = ({ locations }) => {
    return (
      <Fragment>
        {locations.map((location) => (
            <div class="location-card">
              <h4 class="location-title">{location.title}</h4>
              <p>{location.address.address_1} {location.address.address_2},</p>
              <p>{location.address.city_name}, {location.address.country_name}</p>
              <p>Open: {location.open_hour}; Closed: {location.close_hour}</p>
              {location.is_permanent === false &&
                <p>Will be open until: {location.valid_until}</p>
              }
              <p>Organization: {location.organization.title}</p>
              {location.organization.contacts.map((contact) => (
              <p>Contact: {contact.name} - {contact.phone_call_code}({contact.phone_area_code}){contact.phone_prefix}-{contact.phone_line_code}</p>
              ))}
              <hr/>
            </div>
        ))}
      </Fragment>
    )
  };

  export default Locations
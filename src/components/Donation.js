import React, { Component, Fragment } from 'react';
import { Header, Footer } from './Layouts';

class Donantion extends Component {
   render() {
      return (
         <Fragment>
            <Header/>
               <main class="content">
                  <section class="hero">
                     <img class="hero-image" src="assets/donations.png" alt="Financial Donations"/>
                     <div class="hero-right">
                     <div class="hero-content">
                        <h2>Financial Donations</h2>
                        <div class="hr-styled"></div>
                        <p>Find or add opportunities to contribute financial donations online or in person.</p>
                     </div>
                     <img class="hero-flag" src="assets/tt-flag-header.png" alt="TT Flag" />
                     </div>
                  </section>
                  <section class="relief-list">
                     <h3 class="section-title">
                     Red Cross Society of Trinidad and Tobago
                     </h3>
                     <div class="container">
                     <p>The Primary Relief Financial distribution is coordinated by the <strong>Red Cross Society of Trinidad and
                           Tobago</strong>.

                        To donate to the Red Cross please follow these instructions from their office:</p>
                     <div class="spacer"></div>
                     <div class="text-center">
                        <h4>
                           US Dollars
                           Routing Instructions</h4>
                        <p>
                           <strong>Instructions to remit funds<br/>
                           To Republic Bank Limited VIA SWIFT MT103</strong></p>
                     </div>
                     <br/>
                     <table class="table table-sm">
                        <tr>
                           <th>Intermediary Bank</th>
                           <th>FW 026 009 593</th>
                        </tr>

                        <tbody>
                           <tr>
                           <td>(Pay thru bank)</td>
                           <td>Bank of America</td>
                           </tr>
                           <tr>
                           <td>Address</td>
                           <td>
                              100 West 33rd Street<br/>
                              New York<br/>
                              New York 10001<br/>
                              USA<br/><br/>
                              SWIFT Address: BOFAUS3N
                           </td>
                           </tr>
                           <tr>
                           <th>Account with Institution</th>
                           <th>Account No. 65503 52163</th>
                           </tr>

                           <tr>
                           <td>Bank</td>
                           <td>Republic Bank Limited</td>
                           </tr>
                           <tr>
                           <td>Address</td>
                           <td>
                              Park St.<br/>
                              Port of Spain<br/>
                              Trinidad<br/><br/>
                              SWIFT Address: RBNKTTPX</td>
                           </tr>
                           <tr>
                           <th>Beneficiary Account No.</th>
                           <th>180 466 501 801</th>
                           </tr>
                           <tr>
                           <td>Beneficiary</td>
                           <td>Trinidad & Tobago Red Cross Society</td>
                           </tr>
                           <tr>
                           <td>Address</td>
                           <td>
                              7a Fitz Blackman Drive<br/>
                              Wrightson Road Ext.<br/>
                              Port of Spain<br/>
                              Trinidad<br/>
                              West Indies
                           </td>
                           </tr>
                        </tbody>
                     </table>
                     </div>
                     <div class="spacer"></div>
                     <h3 class="section-title">
                     United Way | bmobile
                     </h3>

                     <div class="container">
                     <p><strong>United Way Trinidad and Tobago</strong>, in collaboration with <strong>bmobile</strong>, wants to
                        help our <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/hashtag/citizens?source=feed_text&__xts__%5B0%5D=68.ARDbmOeSva0ucrSNrIAMQh6d9cmsyX2fenDlcgbysjruHgqzNoKOn3oo_zL4MguwsR8VHFY-cYJacuo9YQ-Ycg4pQwZP6EnyPUk8mxhR2EPV9xsWQ6KjA6cOEm3ZdZZ6ANA_l7ES45-JtdqFnqnWJUzwAmqphCW_fARlecqVE1exSy9q2697u06dthazutKPgmG614Mg_rM5l0OIPgNdunmWrvs6z6trz_tM2yfF5IyekkZMvayglIxjSWh_mrI5fTrkoJ3sSWmabI5ckMVo8ProtSpC44xgH6atmIpATOT9JCJ8jef99HkK0RBUA7a6J2Mz09TR3G4ug0pMUKIm5wisbbOwhlUbvbgcZlzR5vzCIG58Cxq2a10rIrpkSaoEMI_Qq1PUK5WG04zA1FQlTPmmQ4sMWHr1tgWmEU2cp2qdLXmKYXkmRoZt5yviYQ0FQD-Yr6yVFvsuWE08e3toTg7FE1D1AjL478kQOT9d5cX0LdeED9zrQLh6L4o8&__tn__=%2ANKH-R">#citizens</a>
                        affected by this
                        disaster. Just TEXT "help" to 6683 and you can <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/hashtag/donate?source=feed_text&__xts__%5B0%5D=68.ARDbmOeSva0ucrSNrIAMQh6d9cmsyX2fenDlcgbysjruHgqzNoKOn3oo_zL4MguwsR8VHFY-cYJacuo9YQ-Ycg4pQwZP6EnyPUk8mxhR2EPV9xsWQ6KjA6cOEm3ZdZZ6ANA_l7ES45-JtdqFnqnWJUzwAmqphCW_fARlecqVE1exSy9q2697u06dthazutKPgmG614Mg_rM5l0OIPgNdunmWrvs6z6trz_tM2yfF5IyekkZMvayglIxjSWh_mrI5fTrkoJ3sSWmabI5ckMVo8ProtSpC44xgH6atmIpATOT9JCJ8jef99HkK0RBUA7a6J2Mz09TR3G4ug0pMUKIm5wisbbOwhlUbvbgcZlzR5vzCIG58Cxq2a10rIrpkSaoEMI_Qq1PUK5WG04zA1FQlTPmmQ4sMWHr1tgWmEU2cp2qdLXmKYXkmRoZt5yviYQ0FQD-Yr6yVFvsuWE08e3toTg7FE1D1AjL478kQOT9d5cX0LdeED9zrQLh6L4o8&__tn__=%2ANKH-R">#donate</a>
                        towards <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/hashtag/helping?source=feed_text&__xts__%5B0%5D=68.ARDbmOeSva0ucrSNrIAMQh6d9cmsyX2fenDlcgbysjruHgqzNoKOn3oo_zL4MguwsR8VHFY-cYJacuo9YQ-Ycg4pQwZP6EnyPUk8mxhR2EPV9xsWQ6KjA6cOEm3ZdZZ6ANA_l7ES45-JtdqFnqnWJUzwAmqphCW_fARlecqVE1exSy9q2697u06dthazutKPgmG614Mg_rM5l0OIPgNdunmWrvs6z6trz_tM2yfF5IyekkZMvayglIxjSWh_mrI5fTrkoJ3sSWmabI5ckMVo8ProtSpC44xgH6atmIpATOT9JCJ8jef99HkK0RBUA7a6J2Mz09TR3G4ug0pMUKIm5wisbbOwhlUbvbgcZlzR5vzCIG58Cxq2a10rIrpkSaoEMI_Qq1PUK5WG04zA1FQlTPmmQ4sMWHr1tgWmEU2cp2qdLXmKYXkmRoZt5yviYQ0FQD-Yr6yVFvsuWE08e3toTg7FE1D1AjL478kQOT9d5cX0LdeED9zrQLh6L4o8&__tn__=%2ANKH-R">#helping</a>
                        victims recover their <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/hashtag/lives?source=feed_text&__xts__%5B0%5D=68.ARDbmOeSva0ucrSNrIAMQh6d9cmsyX2fenDlcgbysjruHgqzNoKOn3oo_zL4MguwsR8VHFY-cYJacuo9YQ-Ycg4pQwZP6EnyPUk8mxhR2EPV9xsWQ6KjA6cOEm3ZdZZ6ANA_l7ES45-JtdqFnqnWJUzwAmqphCW_fARlecqVE1exSy9q2697u06dthazutKPgmG614Mg_rM5l0OIPgNdunmWrvs6z6trz_tM2yfF5IyekkZMvayglIxjSWh_mrI5fTrkoJ3sSWmabI5ckMVo8ProtSpC44xgH6atmIpATOT9JCJ8jef99HkK0RBUA7a6J2Mz09TR3G4ug0pMUKIm5wisbbOwhlUbvbgcZlzR5vzCIG58Cxq2a10rIrpkSaoEMI_Qq1PUK5WG04zA1FQlTPmmQ4sMWHr1tgWmEU2cp2qdLXmKYXkmRoZt5yviYQ0FQD-Yr6yVFvsuWE08e3toTg7FE1D1AjL478kQOT9d5cX0LdeED9zrQLh6L4o8&__tn__=%2ANKH-R">#lives</a>.
                        Text as many times as you wish to donate.<br/><br/>
                        <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/hashtag/liveunited?source=feed_text&__xts__%5B0%5D=68.ARDbmOeSva0ucrSNrIAMQh6d9cmsyX2fenDlcgbysjruHgqzNoKOn3oo_zL4MguwsR8VHFY-cYJacuo9YQ-Ycg4pQwZP6EnyPUk8mxhR2EPV9xsWQ6KjA6cOEm3ZdZZ6ANA_l7ES45-JtdqFnqnWJUzwAmqphCW_fARlecqVE1exSy9q2697u06dthazutKPgmG614Mg_rM5l0OIPgNdunmWrvs6z6trz_tM2yfF5IyekkZMvayglIxjSWh_mrI5fTrkoJ3sSWmabI5ckMVo8ProtSpC44xgH6atmIpATOT9JCJ8jef99HkK0RBUA7a6J2Mz09TR3G4ug0pMUKIm5wisbbOwhlUbvbgcZlzR5vzCIG58Cxq2a10rIrpkSaoEMI_Qq1PUK5WG04zA1FQlTPmmQ4sMWHr1tgWmEU2cp2qdLXmKYXkmRoZt5yviYQ0FQD-Yr6yVFvsuWE08e3toTg7FE1D1AjL478kQOT9d5cX0LdeED9zrQLh6L4o8&__tn__=%2ANKH-R">#LIVEUNITED</a><br/>
                        <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/hashtag/donateforfloodrelief?source=feed_text&__xts__%5B0%5D=68.ARDbmOeSva0ucrSNrIAMQh6d9cmsyX2fenDlcgbysjruHgqzNoKOn3oo_zL4MguwsR8VHFY-cYJacuo9YQ-Ycg4pQwZP6EnyPUk8mxhR2EPV9xsWQ6KjA6cOEm3ZdZZ6ANA_l7ES45-JtdqFnqnWJUzwAmqphCW_fARlecqVE1exSy9q2697u06dthazutKPgmG614Mg_rM5l0OIPgNdunmWrvs6z6trz_tM2yfF5IyekkZMvayglIxjSWh_mrI5fTrkoJ3sSWmabI5ckMVo8ProtSpC44xgH6atmIpATOT9JCJ8jef99HkK0RBUA7a6J2Mz09TR3G4ug0pMUKIm5wisbbOwhlUbvbgcZlzR5vzCIG58Cxq2a10rIrpkSaoEMI_Qq1PUK5WG04zA1FQlTPmmQ4sMWHr1tgWmEU2cp2qdLXmKYXkmRoZt5yviYQ0FQD-Yr6yVFvsuWE08e3toTg7FE1D1AjL478kQOT9d5cX0LdeED9zrQLh6L4o8&__tn__=%2ANKH-R">#DonateForFloodRelief</a><br/>
                        <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/hashtag/868strong?source=feed_text&__xts__%5B0%5D=68.ARDbmOeSva0ucrSNrIAMQh6d9cmsyX2fenDlcgbysjruHgqzNoKOn3oo_zL4MguwsR8VHFY-cYJacuo9YQ-Ycg4pQwZP6EnyPUk8mxhR2EPV9xsWQ6KjA6cOEm3ZdZZ6ANA_l7ES45-JtdqFnqnWJUzwAmqphCW_fARlecqVE1exSy9q2697u06dthazutKPgmG614Mg_rM5l0OIPgNdunmWrvs6z6trz_tM2yfF5IyekkZMvayglIxjSWh_mrI5fTrkoJ3sSWmabI5ckMVo8ProtSpC44xgH6atmIpATOT9JCJ8jef99HkK0RBUA7a6J2Mz09TR3G4ug0pMUKIm5wisbbOwhlUbvbgcZlzR5vzCIG58Cxq2a10rIrpkSaoEMI_Qq1PUK5WG04zA1FQlTPmmQ4sMWHr1tgWmEU2cp2qdLXmKYXkmRoZt5yviYQ0FQD-Yr6yVFvsuWE08e3toTg7FE1D1AjL478kQOT9d5cX0LdeED9zrQLh6L4o8&__tn__=%2ANKH-R">#868Strong</a></p>
                     </div>

                     <div class="spacer"></div>
                     <h3 class="section-title">
                     Habitat For Humanity
                     </h3>

                     <div class="container">
                     <p>For those locally and in the diaspora who would like to help through an online monetary donation, you can
                        donate online to <strong>Habitat for Humanity</strong>.
                        Please support.<br/><br/>
                        Use this link: <a target="_blank" rel="noopener noreferrer" href="https://www.habitat-tt.org/product/coming-home-giving-back/">https://www.habitat-tt.org/product/coming-home-giving-back/</a>.</p>
                     </div>

                     <div class="spacer"></div>
                     <h3 class="section-title">
                     TTMA Flood Relief
                     </h3>

                     <div class="container">
                     <p>An effort to help victims of flooding in T&T. This campaign was established by the Trinidad & Tobago
                        Manufacturer's Association, in the hope of raising TTD$200,000 to help those most in need during this
                        national
                        disaster.</p>

                     <p>
                        Choose your donation method
                     </p>

                     <p><strong>A) Donate <a target="_blank" rel="noopener noreferrer" href="https://fundmetnt.com/campaign/11/tt-flood-relief">here</a>
                           (online) with your
                           Debit / Credit Cards.</strong>
                        <br/><br/>
                        OR
                        <br/><br/>
                        <strong>B) For CASH donations:</strong>
                        <ol>
                           <li>Go to any of the 950 NLCB Lotto Machines in Trinidad & Tobago</li>
                           <li>Say to the NLCB Lotto agent "FundMeTnT" using the Paywise feature and "Campaign ID:219 0123 00005"</li>
                           <li>Make your cash donation.</li>
                        </ol>

                        <br/>
                        Source: <a target="_blank" rel="noopener noreferrer" href="https://fundmetnt.com/campaign/11/tt-flood-relief">https://fundmetnt.com/campaign/11/tt-flood-relief</a>
                     </p>
                     </div>

                  </section>
               </main>
            <Footer/>
         </Fragment>
      );
   }
}

export default Donantion;
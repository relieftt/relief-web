import React, { Component, Fragment } from 'react';
import { Header, Footer } from './Layouts';

class Emergency extends Component {
   render() {
      return (
         <Fragment>
            <Header/>
               <main class="content">
                  <section class="hero">
                     <img class="hero-image" src="assets/emergency.png" alt="Emergency Contacts" />
                     <div class="hero-right">
                        <div class="hero-content">
                           <h2>Emergency Services & Numbers</h2>
                           <div class="hr-styled"></div>
                           <p>
                              A list of emergency numbers and shelters, centres, services and
                              agencies related to your local disaster response.
                           </p>
                        </div>
                        <img class="hero-flag" src="assets/tt-flag-header.png" alt="TT Flag" />
                     </div>
                     </section>
               <section class="relief-list">
                  <h3 class="section-title">Emergency Contacts</h3>
                  <div class="container">
                  <ul>
                     <li>
                        <strong>ODPM Office</strong> - 640 1285
                     </li>
                     <li><strong>ODPM Emergency</strong> - 511</li>
                     <li>
                        <strong>Police Rapid Response</strong> - 999
                     </li>
                     <li>
                        <strong>Global Medical Response of Trinidad and Tobago (GMRTT)
                        </strong>
                        – 811
                     </li>
                     <li>
                        <strong>Tobago Emergency Relief (TEMA)</strong> – 211
                     </li>
                     <li><strong>Ambulance</strong> – 811</li>
                     <li><strong>Fire</strong> – 990</li>
                  </ul>
                  </div>
                  <div class="spacer"></div>
                  <h3 class="section-title">Ministry of Health Emergency Support</h3>
                  <div class="container">
                     <p>
                        Source:
                        <a href="http://www.health.gov.tt/news/newsitem.aspx?id=888" target="_blank"  rel="noopener noreferrer">Ministry of Health</a>
                     </p>
                     <p>
                        Both the La Horquetta Health Center and the Arouca Health Centre have
                        been activated as advanced medical posts.</p>
                     <p>This was done to proactively
                        prepare for the anticipated urgent medical needs of persons in the
                        surrounding areas. Expanded services are now available at these Health
                        Centres and staff are already on site and prepared for a surge in the
                        demand for health care from the public, given the extreme flooding in
                        communities in the area.</p>
                     <p>
                        Members of the public are strongly advised to
                        visit these facilities only if urgent health care is needed so that
                        resources can be directed to persons who are most in need at this
                        time.
                     </p>
                     <p>
                        The Arouca Health Centre is located at: Corner George Street and
                        Golden Grove Road, Arouca
                     </p>
                     <p>
                        The La Horquetta Health Center located at: Arthur Murray Crescent, La
                        Horquetta Phase III
                     </p>
                     <p>
                        Persons who need assistance as advised to use the following emergency
                        contacts:
                     </p>
                     <ul>
                        <li><strong>Police</strong> – 999/511</li>
                        <li><strong>Ambulance</strong> – 811</li>
                        <li><strong>TEMA</strong> – 211</li>
                        <li><strong>Fire</strong> – 990</li>
                     </ul>
                  </div>
                  <div class="spacer"></div>
                  <h3 class="section-title">Other Links</h3>
                  <div class="container">
                     <ul>
                        <li>
                           <a href="https://www.meppublishers.com/trinidad-tobago-floods-2018-how-to-help/?fbclid=IwAR0OKBJJUqxbs8lTpxJ3WZ7kuqhIewpCDI-wtXFM06v29D365mSM9HNvN3k#ixzz5UWBjhYzz"
                           target="_blank"  rel="noopener noreferrer">MEP
                           Publishers</a>
                        </li>
                        <li>
                           <a href="https://bit.ly/2S7wCYK" target="_blank"  rel="noopener noreferrer">LoopTT - List of Donation Drop Off Points</a>
                        </li>
                        <li>
                           <a href="http://www.odpm.gov.tt/node/82" target="_blank"  rel="noopener noreferrer">ODPM - Regional Corporation Hotlines</a>
                        </li>
                     </ul>
                  </div>
               </section>
               </main>
            <Footer/>
         </Fragment>
      );
   }
}

export default Emergency;
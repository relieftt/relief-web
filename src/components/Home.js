import React, { Component, Fragment } from 'react';
import { Header, Footer } from './Layouts';
import Locations from './Locations';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';


class Home extends Component {
   constructor(props) {
      super(props);
      this.state = {
        lat: 10.5918,
        lng: -61.2225,
        zoom: 9,
        locations: []
      };
    }

    componentDidMount() {
      fetch('http://localhost:8000/api/v1/location/unfiltered/') //NOTE: Change in production
      .then(res => res.json())
      .then((data) => {
        this.setState({ locations: data })
      })
      .catch(console.log)
    }

   render() {
      const position = [this.state.lat, this.state.lng]
      return (
         <Fragment>
            <Header/>
               <main class="content">
                  <h2>Relief can start here</h2>
                  <section class="features">
                     <a class="feature-link" href="/#dropoff">
                        <div class="feature-icon bg-warning">
                           <img src="assets/icons/map-icon.png" alt="Drop Off Location Icon"/>
                        </div>
                        <h3>Drop Off<br/>Locations</h3>
                     </a>
                     <a class="feature-link" href="/emergency">
                        <div class="feature-icon bg-warning">
                           <img src="assets/icons/contact-icon.png" alt="Emergency Contacts Icon"/>
                        </div>
                        <h3>Emergency<br/>Contacts</h3>
                     </a>
                     <a class="feature-link" href="/services">
                        <div class="feature-icon bg-warning">
                           <img src="assets/icons/settings-icon.png" alt="Services Icon"/>
                        </div>
                        <h3>Services for<br/>Victims</h3>
                     </a>
                     <a class="feature-link" href="/donation">
                        <div class="feature-icon bg-warning">
                           <img src="assets/icons/donation-icon.png" alt="Donation Icon"/>
                        </div>
                        <h3>Donation<br/>Options</h3>
                     </a>
                  </section>
                  <section id="dropoff" class="locations-section">
                     <div class="row justify-content-center search-section">
                           <div class="col-12 col-md-10 col-lg-8">
                              <form class="card card-sm">
                                 <div class="card-body row no-gutters align-items-center">
                                 <div class="col">
                                    <div class="form-group">
                                       <label class="form-label"><small>Search either by direction, town or place</small></label>
                                       <div class="input-group">
                                          <input type="text" id="relief-search" class="form-control form-control-lg" placeholder="e.g. South, San Fernando, Movietowne etc.." />
                                          <button id="search-clear" class="btn bg-transparent">
                                             <i class="fa fa-times"></i>
                                          </button>
                                       </div>
                                    </div>
                                 </div>
                                 </div>
                           </form>
                           </div>
                     </div>
                  <div class='row location-widget search-results'>
                     <div class="col-lg-6 col-sm-12 order-lg-2 px-0">
                        <div id="listHolder" class="list-holder locations-col">
                           <Locations locations={this.state.locations} />
                        </div>
                     </div>
                     <div class="col-lg-6 col-sm-12 order-lg-2 px-0">
                        <div id="map" class="map">
                           <Map center={position} zoom={this.state.zoom}>
                              <TileLayer
                                 attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                                 url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
                              <Marker position={position}>
                                 <Popup>
                                    A pretty CSS3 popup. <br /> Easily customizable.
                                 </Popup>
                              </Marker>
                           </Map>
                        </div>
                     </div>
                  </div>
                  </section>
                  <section class="image"><img src="assets/volunteer.png" alt="Volunteers"/></section>
               </main>
            <Footer/>
         </Fragment>
      );
   }
}

export default Home;